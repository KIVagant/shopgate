<?php
require_once '../src/config.php';
require_once '../src/functions.php';

$content = main_controller($background);

// make cache
file_put_contents('index.html', $content);
echo $content;
