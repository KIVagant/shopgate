<?php
define('TEMPLATE_MAIN', '<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>List of thumbnails</title>
</head>
<body>
<ol>
    {{$list}}
</ol>
</body>
</html>');
define('TEMPLATE_IMAGE', '<li><img src="{{$url}}" /></li>');
define('IMAGE_SCALE', 600);
define('PATH_TO_ORIGINALS', '../resources/images.dat');
define('PATH_TO_COMPRESSED', 'images/');
$background = [255, 255, 255];