<?php

function create_template_image($background)
{
    $image_template = imagecreatetruecolor(IMAGE_SCALE, IMAGE_SCALE);
    $image_color = imagecolorallocate($image_template, $background[0], $background[1], $background[2]);
    imagefill($image_template, 0, 0, $image_color);

    return $image_template;
}

function image_zoom_out_size($width, $height)
{
    if ($width > IMAGE_SCALE) {
        $new_width = IMAGE_SCALE;
        $ratio = $new_width / $width;
        $new_height = $ratio * $height;
        $width = $new_width;
        $height = $new_height;
    }

    if ($height > IMAGE_SCALE) {
        $new_height = IMAGE_SCALE;
        $ratio = $new_height / $height;
        $new_width = $ratio * $width;
        $width = $new_width;
        $height = $new_height;
    }

    return [round($width), round($height)];
}


function get_new_filename($stream, $url)
{
    $hash = md5($stream);
    $name = $hash . '.' . basename($url);

    return $name;
}

function get_images_urls()
{
    $images_list = file_get_contents(PATH_TO_ORIGINALS);
    $images_list = explode("\n", $images_list);

    return $images_list;
}

function calc_thumb_dst_coords($new_width, $new_height)
{
    $dst_x = 0 - ($new_width - IMAGE_SCALE) / 2;
    $dst_y = 0 - ($new_height - IMAGE_SCALE) / 2;

    return array($dst_x, $dst_y);
}

function save_image($type, $name, $image_template)
{
    switch ($type) {
        case IMAGETYPE_JPEG:
            imagejpeg($image_template, PATH_TO_COMPRESSED . $name);
            break;
        case IMAGETYPE_PNG:
            imagepng($image_template, PATH_TO_COMPRESSED . $name);
            break;
    }
}

function load_and_create_thumb($url, $background)
{
    $stream = @file_get_contents($url);
    if (!$stream) {

        throw new \Exception('Unable to load resource file');
    }
    $name = get_new_filename($stream, $url);
    list($original_width, $original_height, $type) = getimagesizefromstring($stream);
    list($new_width, $new_height) = image_zoom_out_size($original_width, $original_height);
    $image = imagecreatefromstring($stream);
    list($dst_x, $dst_y) = calc_thumb_dst_coords($new_width, $new_height);
    $image_template = create_template_image($background);
    imagecopyresampled($image_template, $image, $dst_x, $dst_y, 0, 0, $new_width, $new_height, $original_width,
        $original_height);
    save_image($type, $name, $image_template);

    return $name;
}

function parse_template($var, $content, $template)
{
    $content = str_replace('{{' . $var . '}}', $content, $template);

    return $content;
}

function main_controller($background)
{
    $images_list = get_images_urls();
    $new_images_list = [];
    foreach ($images_list as $url) {
        try {
            $image_name = load_and_create_thumb($url, $background);
            $new_images_list[$url] = parse_template('$url', PATH_TO_COMPRESSED . $image_name, TEMPLATE_IMAGE);

        } catch (\Exception $e) {
            // some error reporting
        }
    }
    $content = parse_template('$list', implode(PHP_EOL . "\t", $new_images_list), TEMPLATE_MAIN);

    return $content;
}